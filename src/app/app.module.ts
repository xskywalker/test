import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//page baru
import { PricingModule } from './pages/pricing/pricing.module';
import { MainpageModule } from './section/mainpage/mainpage.module';



import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ThankyouModule } from './pages/thankyou/thankyou.module';


@NgModule({
  declarations: [
    AppComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    //module baru 
    MainpageModule,
    PricingModule,
    ThankyouModule,
    HttpClientModule,
    ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
