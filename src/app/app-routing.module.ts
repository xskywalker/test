import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PricingComponent } from './pages/pricing/pricing.component';
import { ThankyouComponent } from './pages/thankyou/thankyou.component';
import { MainpageComponent } from './section/mainpage/mainpage.component';


const routes: Routes = [
  //untuk panggil page di navigation bar

  { 
    path: '', 
    component:MainpageComponent
  },
  //path ke pricing page
  { 
    path: 'pricing', 
    component:PricingComponent
  },
  //path ke thankyou
  { 
    path: 'thankyou', 
    component:ThankyouComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
