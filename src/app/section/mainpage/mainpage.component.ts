import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {

  loginUserData = {
    email: '',
    password: ''
  }
  

  constructor(private http:HttpClient) { }

  ngOnInit(): void {
  }

  //method bila button click
  loginUser(){
    console.log(this.loginUserData)
    
    
  //pattern untuk post ke res API
  this.http.post('https://demo.monthley.com/rest/auth', this.loginUserData, {headers:new HttpHeaders({'Content-Type':'application/json'})}).subscribe(
    (response: any) => console.log(response,"userresponse"),
    (error: any) => console.log(error)
  )

  }
  

  

  

}
